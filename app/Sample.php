<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sample extends Model
{
    //
	protected $table = 'widgets';
	protected $fillable = ['name'];
}
