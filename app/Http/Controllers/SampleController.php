<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sample;
use DB;
class SampleController extends Controller
{
    function index(){
	 	echo "Thanks for the opportunity"; 
	}
	
	function widget_save(Request $request){
		$json_array 					= array("status"=>"failed");
		if(!empty($request->all())){
			$widget_object					= new Sample;
			$widget_object->name			= $request->input('name');
			$qry							= $widget_object->save();
			if($qry){
				$json_array 				= array('status'=>'success','message'=>'New widget name added successfully'); 
			}
		}
		echo json_encode($json_array);
		exit;
	}
	
	function widget_update(Request $request,$id= null){
		$json_array = array("status"=>"failed");
		$widget_details = array();
		if(!empty($request->all())){
			if($id){
				$widget_object					= new Sample;
				$widget_details 				= $widget_object->where('id',$id)->get()->toArray();
				if(!empty($widget_details)){
					$update_details 			= array();
					$update_details['name']		= $request->input('name');
					$qry 						= $widget_object->where('id',$id)->update($update_details);
					if($qry){
						$json_array 			= array('status'=>'success','message'=>'Name updated successfully'); 
					}else{
						$json_array 			= array('status'=>'failed','message'=>'Updation failed'); 
					}
				}else{
					$json_array 				= array('status'=>'failed','message'=>'No data found'); 
				}
			}
		}
		echo json_encode($json_array);
		exit;
	}
	
	function widget_details($id = null ){
		$details = array();
		$json_array = array();
		$widget_object		 = new Sample;
		if($id){
			$details 		 = $widget_object->where('id',$id)->get()->toArray();
		}else{
			$details 		 = $widget_object->get()->toArray();
		}
		if($details && !empty($details)){
			$json_array = array("status"=>"sucess",'message'=>$details);
		}else{
			$json_array = array("status"=>"failed");
		}
		echo json_encode($json_array);
		exit;
	}
	
	function currency_details($pageNumber = null){
		$pagelimit = 50;
		$json_array  = array('Message'=>'failed');
		if(isset($pageNumber) && $pageNumber !=''){
			$string = file_get_contents("https://min-api.cryptocompare.com/data/top/mktcapfull?limit=".$pagelimit."&page=".($pageNumber-1)."&tsym=EUR&api_key=b40dd40f80da4fc24a8f2159c5c99e427b266a72727b945a3d29ad6bc4b989cb");
		}else{
			$string = file_get_contents("https://min-api.cryptocompare.com/data/top/mktcapfull?limit=".$pagelimit."&page=0&tsym=EUR&api_key=b40dd40f80da4fc24a8f2159c5c99e427b266a72727b945a3d29ad6bc4b989cb");
		} 
		$json_array = json_decode($string, true);
		echo json_encode($json_array);		
		exit;
	}
}
