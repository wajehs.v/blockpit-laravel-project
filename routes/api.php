<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('currencies/{id?} ','SampleController@currency_details');
Route::get('widgets','SampleController@widget_details');
Route::get('widgets/{id?}','SampleController@widget_details');

Route::put('widgets/{id?}','SampleController@widget_update');

Route::post('widgets','SampleController@widget_save');

Route::get('/db-test', 'Controller@testDB');
